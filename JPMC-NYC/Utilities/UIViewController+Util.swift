//
//  UIViewController+Util.swift
//  JPMC-NYC
//
//  Created by Ayesha Shaikh on 3/14/23.
//

import Foundation
import UIKit

/*
 * UIViewController Extension methods
 */
extension UIViewController {
    
    /// Method to show error alert view on a view controller
    ///
    /// - Parameters:
    ///   - message: Error message
    func showError(with message: String) {
        let alert = UIAlertController(title: "Oops", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
