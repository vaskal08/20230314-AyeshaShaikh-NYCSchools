//
//  SATResult.swift
//  JPMC-NYC
//
//  Created by Ayesha Shaikh on 3/14/23.
//

import Foundation

// MARK: - SATResult

struct SATResult : Codable {
    let dbn : String?
    let school_name : String?
    let num_of_sat_test_takers : String?
    let sat_critical_reading_avg_score : String?
    let sat_math_avg_score : String?
    let sat_writing_avg_score : String?

    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case school_name = "school_name"
        case num_of_sat_test_takers = "num_of_sat_test_takers"
        case sat_critical_reading_avg_score = "sat_critical_reading_avg_score"
        case sat_math_avg_score = "sat_math_avg_score"
        case sat_writing_avg_score = "sat_writing_avg_score"
    }
}

/**
 Extension for SATResult model
 */
extension SATResult {
    
    /**
     Check for empty and nil values
     */
    func isNotEmptyOrNil() -> Bool {
        guard let sat_math_avg_score = sat_math_avg_score,
                !sat_math_avg_score.isEmpty,
                let sat_writing_avg_score = sat_writing_avg_score,
                !sat_writing_avg_score.isEmpty,
                let sat_critical_reading_avg_score = sat_critical_reading_avg_score,
                !sat_critical_reading_avg_score.isEmpty else { return false }
        return true
        
    }
}
