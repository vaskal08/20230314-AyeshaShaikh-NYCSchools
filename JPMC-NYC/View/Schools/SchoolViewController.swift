//
//  SchoolViewController.swift
//  JPMC-NYC
//
//  Created by Ayesha Shaikh on 3/14/23.
//

import Foundation
import UIKit

/**
 Home screen to display list of schools in NYC
 */
class SchoolViewController: UIViewController {
    
    private let viewModel: SchoolViewModel
    private let tableView: UITableView
    private let activityView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
    
    init() {
        tableView = UITableView()
        viewModel = SchoolViewModel(apiManager: APIManager())
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Schools in NYC"
        navigationItem.backButtonDisplayMode = .minimal
        view.backgroundColor = .white
        setupView()
        setupData()
    }
    
    private func setupView() {
        activityView.center = self.view.center
        self.view.addSubview(activityView)
        activityView.startAnimating()
        
        tableView.register(SchoolRow.self, forCellReuseIdentifier: String(describing: SchoolRow.self))
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.backgroundColor = .white
        view.addSubview(tableView)
        view.embed(childView: tableView, in: view,
                   insets: UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0))
    }
    
    private func setupData() {
        viewModel.getListOfSchools { [weak self] apiError in
            guard let self = self else { return }
            DispatchQueue.main.async {
                
                self.activityView.stopAnimating()
                self.activityView.removeFromSuperview()
                
                if let apiError = apiError {
                    self.showError(with: apiError.message)
                } else {
                    self.tableView.reloadData()
                }   
            }
        }        
    }
}

/**
 Extension for tableview Delegate methods
 */
extension SchoolViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let schoolDetailViewModel = SchoolDetailViewModel(apiManager: APIManager(),
                                                          school: viewModel.school(for: indexPath))
        let schoolDetailViewController = SchoolDetailViewController(with: schoolDetailViewModel)        
        self.navigationController?.pushViewController(schoolDetailViewController, animated: true)        
    }
}

/**
 Extension for tableview datasource methods
 */
extension SchoolViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SchoolRow.self), for: indexPath) as? SchoolRow else {
            fatalError("TableView was unable to return a School row")
        }
        cell.configure(with: viewModel.school(for: indexPath))
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
}
