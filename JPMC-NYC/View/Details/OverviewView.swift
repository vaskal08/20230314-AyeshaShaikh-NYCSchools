//
//  OverviewView.swift
//  JPMC-NYC
//
//  Created by Ayesha Shaikh on 3/14/23.
//

import Foundation
import UIKit

/**
  View to display overview section
 */
class OverviewView: UIView {
    
    private var titleLabel = UILabel()
    private var overviewLabel = UILabel()
    
    // MARK: - Init Methods
    
    init(with school: School) {
        super.init(frame: .zero)
        
        setup()
        updateView(with: school)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private Methods
    
    private func setup() {
        layer.borderColor = UIColor.black.cgColor
        layer.cornerRadius = 10
        layer.borderWidth = 2
        
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 5.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
        embed(childView: stackView, in: self,
              insets: UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0))
        
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = "Overview"
        titleLabel.font = .boldSystemFont(ofSize: 14.0)
        stackView.addArrangedSubview(titleLabel)
        
        overviewLabel.translatesAutoresizingMaskIntoConstraints = false
        overviewLabel.numberOfLines = 0
        overviewLabel.lineBreakMode = .byWordWrapping
        overviewLabel.font = .systemFont(ofSize: 14.0)
        stackView.addArrangedSubview(overviewLabel)
    }
    
    // MARK: - Public Methods
    
    func updateView(with school: School) {
        overviewLabel.text = school.overviewParagraph
    }
}
