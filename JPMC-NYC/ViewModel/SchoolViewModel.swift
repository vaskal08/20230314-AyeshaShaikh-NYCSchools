//
//  SchoolViewModel.swift
//  JPMC-NYC
//
//  Created by Ayesha Shaikh on 3/14/23.
//

import Foundation

/**
 View model class for Home view controller to display list of schools
 */
class SchoolViewModel {
    
    var apiManager: APIManager
    var allSchools: [School]
    
    // MARK: - Init
    
    init(apiManager: APIManager) {
        self.apiManager = apiManager
        allSchools = []
    }
    
    // MARK: - API Interface
    
    func getListOfSchools(completion: @escaping (APIError?) -> ()) {
        guard let url = EndPoint().urlForListOfSchools() else {
            completion(.invalidRequest)
            return
        }
        self.apiManager.fetch(from: url, of: [School].self) { [weak self] result in
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
                completion(error)
                
            case .success(let schools):
                guard let self = self else { return }
                self.allSchools.append(contentsOf: schools)
                completion(nil)
            }
        }
    }
    
    // MARK: - Helper Methods
    
    func numberOfRows() -> Int {
        allSchools.count
    }
    
    func school(for index: IndexPath) -> School {
        allSchools[index.row]        
    }
}
