//
//  APIManager.swift
//  JPMC-NYC
//
//  Created by Ayesha Shaikh on 3/14/23.
//

import Foundation
import UIKit
import Network

/**
 Possible errors for the API request
 */
enum APIError: Error {
    case invalidResponse
    case invalidData
    case invalidDecoding
    case invalidRequest
    case noInternetConnection
    
    var message: String {
        switch self {
        case .invalidResponse:
            return "Invalid response from Server"
        case .invalidData:
            return "Data is invalid"
        case .invalidDecoding:
            return "Erroe decoding the data"
        case .invalidRequest:
            return "The request is not valid"
        case .noInternetConnection:
            return "No internet connection"
        }
    }
}

/**
 Return urls
 */
struct EndPoint {
    
    let scheme = "https"
    let host = "data.cityofnewyork.us"
    
    func urlForListOfSchools() -> URL? {
        var components = URLComponents()
        components.scheme = scheme
        components.host = host
        components.path = "/resource/s3k6-pzi2.json"
        return components.url
    }
    
    func urlForSATScore(for schoolDbn: String) -> URL? {
        let queryItems = [URLQueryItem(name: "dbn", value: schoolDbn)]
        
        var components = URLComponents()
        components.scheme = scheme
        components.host = host
        components.path = "/resource/f9bf-2cp4.json"
        components.queryItems = queryItems
        return components.url
    }
}

/**
 Make network calls
 */
class APIManager {
    
    let monitor = NWPathMonitor()
    
    /// Constraints to embed child view into parent view with given insets
    ///
    /// - Parameters:
    ///   - url: URL
    ///   - type: Model object
    ///   - completion: completion block
    func fetch<T: Decodable>(from url: URL, of type: T.Type, completion: @escaping (Result<T, APIError>) -> Void) {
        monitor.pathUpdateHandler = { path in
            if path.status != .satisfied {
                completion(.failure(.noInternetConnection))
                return
            }
        }
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard error == nil else {
                completion(.failure(.invalidRequest))
                return
            }
            guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
                completion(.failure(.invalidResponse))
                return
            }
            guard let data = data else {
                completion(.failure(.invalidData))
                return
            }
            do {
                let json = try JSONDecoder().decode(T.self, from: data)
                completion(.success(json))
            } catch {
                completion(.failure(.invalidDecoding))
            }
            
        }.resume()
    }
}


