//
//  JPMC_NYCTests.swift
//  JPMC-NYCTests
//
//  Created by Ayesha Shaikh on 3/14/23.
//

import XCTest
@testable import JPMC_NYC

class JPMC_NYCTests: XCTestCase {
    
    let apiManager = APIManager()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func test_URL_isValid() {
        let schoolsUrl = EndPoint().urlForListOfSchools()?.absoluteString.isEmpty ?? false
        XCTAssertFalse(schoolsUrl, "Schools URL is not empty")
        
        let satScoresUrl = EndPoint().urlForSATScore(for: "02M408")?.absoluteString.isEmpty ?? false
        XCTAssertFalse(satScoresUrl, "SAT school URL is not empty")
    }

    func test_fetchSchoolList() {
        let url = EndPoint().urlForListOfSchools()
        apiManager.fetch(from: url!, of: [School].self) { result in
            switch result {
            case .success(let schools):
                XCTAssertNotNil(schools, "Yay")
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }
    }
    
    func test_fetchSATScores() {
        let url = EndPoint().urlForSATScore(for: "02M408")
        apiManager.fetch(from: url!, of: [SATResult].self) { result in
            switch result {
            case .success(let satScore):
                XCTAssertNotNil(satScore, "Yay")
            case .failure(let error):
                XCTFail(error.localizedDescription)
            }
        }
    }
    
    
}
