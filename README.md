# 20230314 AyeshaShaikh NYCSchools



## REQUIREMENTS
 
The app requirements are rather high-level and vague. If details are omitted, it is because we will be  happy with any of a wide variety of functional solutions. Feel free to be creative and impress with  code and UX (see more details down). You are allowed to use all the online resources you can find  and use any 3rd party library if desired. Remember to make it useful for an actual user/student. 
Create a native app to provide information on NYC High schools. 
1. Display a list of NYC High Schools.  
1. Get your data here: https://data.cityofnewyork.us/Education/DOE-High-SchoolDirectory-2017/s3k6-pzi2 
2. Selecting a school should show additional information about the school  
1. Display all the SAT scores - include Math, Reading and Writing.  
1. SAT data here: https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4 
2. It is up to you to decide what additional information to display 

## What to expect?

1. Land on a Home page that shows a list of Schools from the given URL.
2. Tap on each school to navigate to a Detail screen, wth information for SAT scores, overview of the school and Contact section
3. Contact section has links for Map, Phone and Website, clicking on them will take you to the respective destinations. 

## Screenshots

Click on below links to open a preview of the app

![Home](https://gitlab.com/ayeshabs91/20230314-AyeshaShaikh-NYCSchools/-/blob/470dad859aeab4b79aec183642392f84dc04294f/Screen%20Shot%202023-03-15%20at%2012.32.06%20AM.png "Home Screen")


![Detail](https://gitlab.com/ayeshabs91/20230314-AyeshaShaikh-NYCSchools/-/blob/470dad859aeab4b79aec183642392f84dc04294f/Screen%20Shot%202023-03-15%20at%2012.33.01%20AM.png "Detail Screen")

